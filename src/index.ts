import type { IndexHtmlTransformResult, HtmlTagDescriptor } from 'vite'

export default ({
    debug,
    eruda_src,
  } : {
    debug?: boolean | undefined,
    eruda_src?: string
  }= {
    debug: undefined,
    eruda_src: 'https://unpkg.com/eruda@2.4.1/eruda.js'
  }) => {
    return {
      name: "vite-plugin-eruda",
      transformIndexHtml(html: string): IndexHtmlTransformResult {
        const tags: HtmlTagDescriptor[] = [
          {
            tag: "script",
            attrs: {
              src: eruda_src
            },
            injectTo: "head"
          },
          {
            tag: "script",
            children: "eruda.init();",
            injectTo: "head"
          }
        ];
        if (debug === true) {
          return {
            html,
            tags
          };
        } else if (debug === false) {
          return html;
        }
        if (process.env.NODE_ENV !== "production") {
          return {
            html,
            tags
          };
        } else {
          return html;
        }
      }
    };
  }