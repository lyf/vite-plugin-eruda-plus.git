# Vite Plugin Eruda

> A vite plugin help you automatically open debugging tools in the development environment

> Modified from https://www.npmjs.com/package/vite-plugin-eruda

### Install
```sh
$ npm i --save-dev vite-plugin-eruda-plus
```

### Usage
```javascript
import eruda from 'vite-plugin-eruda-plus'


module.exports = {
  plugins: [
    // others
    eruda()
  ]
}
```

### Options

#### `debug`

- **Type:** `boolean | undefind`
- **Default:** `'undefind'`

#### `eruda_src`

- **Type:** `string`
- **Default:** `'https://unpkg.com/eruda@2.4.1/eruda.js'`


  Optional. If not, process.env.node will be used by default `process.env.NODE_ENV !== "production"` standard opens the debugging mode. If there is, this parameter takes precedence.

### License
MIT
